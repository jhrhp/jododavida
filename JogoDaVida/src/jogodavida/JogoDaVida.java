package jogodavida;

public class JogoDaVida {

    static String[][] matriz = {
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"}};

    static String[][] matriz2 = {
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"},
        {"_", "_", "_", "_", "_"}};

    public static String[][] troca(String[][] matriz, int c, int l) {

        int vizinhas = 0;

        for (int i = 0, l2 = 1 - 1; i < 3; i++, l2++) {
            for (int j = 0, c2 = c - 1; j < 3; j++, c2++) {
                if(c == i && l == j){
                    continue;
                }
                try {
                    if ("*".equals(matriz[i][j])) {
                        vizinhas++;
                    }

                } catch (Exception e) {
                    continue;
                }
            }

        }

        /*if (vizinhas > 3 || vizinhas < 2) {
            matriz2[c][l] = "_";
        } else if(vizinhas == 3 ){
            matriz2[c][l] = "*";
        } else {
            matriz2[c][l] = "_";
        }*/

        
        if(vizinhas > 3 && matriz [c][l].equals("*") ) {
            matriz2[c][l] = "_";
        }else if(vizinhas == 3 && matriz [c][l].equals("_"))
            matriz2[c][l] = "*";        
        if(vizinhas < 2 && matriz [c][l].equals("*")) {
            matriz2[c][l] = "_";            
        }
         
        return matriz2;
    }
    
    public static String[][] tick() {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                //if (matriz[i][j].equals("*")) {
                    matriz2 = tick(matriz, i, j);
                //}
            }
        }
    }

    public static void main(String[] args) {
        matriz[1][2] = "*";
        matriz[2][2] = "*";
        matriz[3][2] = "*";

        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                //if (matriz[i][j].equals("*")) {
                    tick(matriz, i, j);
                //}
            }
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(matriz2[i][j]);
            }
            System.out.println("");
        }

    }

}
