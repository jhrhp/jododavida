/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.time.Clock;
import java.time.Duration;
import jogodavida.JogoDaVida;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sasuke
 */
public class JogoDaVidaTest {

    @Test
    public void testaInicial() {
        String[][] matriz = {
            {"_", "_", "_", "_", "_"},
            {"_", "_", "*", "_", "_"},
            {"_", "_", "*", "_", "_"},
            {"_", "_", "*", "_", "_"},
            {"_", "_", "_", "_", "_"}};

        String[][] matrizResultado = {
            {"_", "_", "_", "_", "_"},
            {"_", "_", "_", "_", "_"},
            {"_", "_", "*", "_", "_"},
            {"_", "_", "*", "_", "_"},
            {"_", "_", "_", "_", "_"}};
        
        assertEquals(matrizResultado, JogoDaVida.tick(matriz, 1, 2));

    }
}
